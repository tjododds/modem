from scipy.io.wavfile import read, write
import numpy as np
import matplotlib.pyplot as plt

def createKeys(numberFreq, sampleRate, freqLim=25000):
    divFactor = sampleRate/(np.pi*2)
    keys = []
    curDiv = 0.01
    for i in range(numberFreq):
        keys.append(divFactor*curDiv)
        curDiv = curDiv / 2
    return keys

def createFrame(tick, data, keys, frameWidth=10):
    frame = []
    print(len(data),len(keys))
    while len(data) < len(keys):
        data = data + '0'
    for i in range(frameWidth):
        currentFrame = 0.0
        for a in range(len(keys)):
            if data[a] == '1':
                currentFrame = currentFrame + np.sin(tick/keys[a])
        tick = tick+1
        frame.append((2**14)*currentFrame)
    return frame
