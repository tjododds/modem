from __future__ import absolute_import, division, print_function, unicode_literals
from scipy.io.wavfile import read,write
import numpy as np
import musicMaker as mm
import random

def smoothWave(data):
    returnValue = []
    for i in range(len(data)):
        if i < 2 :
            returnValue.append(data[i])
        else:
            returnValue.append([data[i][0]/2,data[i][1]/2])
    return np.array(returnValue,dtype=np.int16)
##    return returnValue


a = read("test.wav")
# print(mm.createKeys(20,a[0]))
# b = np.array(a[1], dtype=np.int16)
# print(b[10:20])
# b = smoothWave(b)
# print(b[10:20])
# print(a[0])

def decodeBinaryToString(binaryToDecode):
    b = int("0b" + binaryToDecode,2)
    return b.to_bytes((b.bit_length() + 7) // 8, 'big').decode()
def decodeStringToBinary(stringToDecode):
    string = bin(int.from_bytes(stringToDecode.encode(), 'big'))[2:]
    return string

def createData(width):
    data=""
    for i in range(width):
        data = data + str(random.getrandbits(1))
    return data

def createSignal(dWidth, sWidth, sLength, binaryData = ""):
    signal = []
    ticks = 0
    for i in range(sWidth):
        if binaryData != "":
            signal = signal + mm.createFrame(ticks,binaryData[i*dWidth:(i+1)*dWidth],keys, sLength)
        else:
            signal = signal + mm.createFrame(ticks,createData(dWidth),keys, sLength)
        ticks = ticks + sLength
    return signal

keys = mm.createKeys(4,4200)
print(decodeBinaryToString(decodeStringToBinary("I Like Cheese")))
frames = np.array(createSignal(4,50,4200,decodeStringToBinary("以下展示了使用 chr() 方法的实例")),dtype=np.int16)
print(frames)
write("output.wav",a[0],frames)


import collections
import matplotlib.pyplot as plt
